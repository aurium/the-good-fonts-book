PDFREADER=evince
book.tex:
	python generate_tex_from_fontbakery_issues.py
	
book.pdf: book.tex
	pdflatex --shell-escape book.tex

all: book.pdf
	$(PDFREADER) book.pdf

clean:
	rm book.pdf book.tex

