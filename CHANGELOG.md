# 6th July, 2021 04:31am (GMT-3) - Felipe Sanches
## How I came up with this idea:
* https://www.theatlantic.com/technology/archive/2021/06/the-internet-is-a-collective-hallucination/619320/
* Got the URL to this article a couple hours ago at a Telegram chat-group where a group of 106 Brazilian friends share dad-jokes on a regular basis.
* https://github.com/googlefonts/fontbakery/issues/2536 - "Request for comments: Move checks/tests to a literate programming format" - davelab6 opened this issue on Jun 3, 2019 (after a nice conversation / brainstorm on a train ride in Europe, for a TypeConference or maybe for Libre Graphics Meeting?)
* Lately I have been interested in LaTeX once again. I did write some simple TeX code to render lyrics and chords for the song "Never Gonna Let You Go" last week (https://github.com/felipesanches/NeverGonnaLetYouGo), after watching this Rick Beato video on Youtube: "The Most COMPLEX Pop Song of All Time" (https://www.youtube.com/watch?v=ZnRxTW8GxT8)
* I have been working all day today and also a few hours in the weekend gathering links to GitHub issues or pull requests where FontBakery checks have been originally proposed (or submitted without a more thorough brainstorming first).
* Once I finish gathering all links, I think I'll be able to automate the submission of these URLs to the Wayback Machine. But I'll also be able to automatically download my own copy of all those github issue tracker entries.
* Back in March 2011, during the LibrePlanet conference in Boston, I met an MIT student called Christine Spang. She told me about a distributed issue tracker. I've been since then thinking about that idea from time to time. On my daily work, I often wish FontBakery issue tracker content was kept on a git repo, or something similar to that.
* We keep caches of content such as the vendor ID list from the Microsoft website. We could do the same for our own content.
* FontBakery is a knowledge gathering project. Even though we've been gradually collecting "rational" metadata for the checks, the discussions that lead to the proposal of a new check, are often much richer than the source-code itself.
* Once I have some sort of structured data cached form the fontbakery issue tracker, I think I'll be able to generate LaTeX sources for a thorough Font Bakery check documentation PDF that one could print and hold in hands saying: "This in the entirety of the knowledge collected by the Font Bakery project up to now!".
* The raw log of those issues and pull requests would look more like a book appendix.
* The introduction would likely be made using the current documentation that we have hosted at https://font-bakery.readthedocs.io/en/latest/
* I've been trying to fix a bug in the docs by the way: https://github.com/googlefonts/fontbakery/issues/3313
* The implementation of the checks change over time, as criteria evolve or are better clarified.
* The rationale metadata is meant to be not too long, as it must be practical for the users to quickly grasp the core of the problem, and eventually some quick tips on how to fix them on ther font files.
* So, there seems to be a need for a more detailed description of the problems detected by FontBakery checks, perhaps as a better-written overview of everything that has been discussed chaotically on the issue tracker (and is instant messaging chats, and videocalls with the team at Google Fonts or with typedesigners who're interested in using the tool)
* I think I may write a book about FontBakery. (I always knew I'd write a book someday. Never though it would be about fonts, though :-P)

## Some more insights:
* Indeed, everything is connected! There are so many interconnected ideas in my head right now!
* I've been a supporter of the Internet Archive for a long time (and even looked for a job opportunity there many years ago). I met Brewster Khale in person during the Chaos Communication Camp back in 2011.
* I also met Jonnathan Zittrain (the author of that article on The Atlantic) circa 2010 at the local hackerspace (Garoa Hacker Clube) that I helped co-found and have been running in São Paulo, Brazil.
* What's the license of the content of an issue tracker on GitHub? I believe it might be implicitely the same license as the project, but I am sure other people might disagree. Google does have a CLA (Contributor License Agreement), but that's only required to be signed when someone sends a pull request. As far as I can tell, that's not required for participation on the issue tracker.
* There's been many cases of full paragraphs of issue tracker comments being copied almost verbatim into the checks' rationale metadata on the FB source code. Nobody seems to complain about that.
* If I actually write a book, all those people would have to be recognized as contributors (sort of co-authors to some extent, right?). Also, the Apache license (which is what FB uses) would be OK for publishing a book? I think so...

## I've been thinking a bit more:
* Aurelio Heckert have been working for me, helping with some small tasks on Font Bakery on a sporadic freelancer.
* I think I should talk to him and propose some tasks related to this book-idea.
* For instance, he could help me setup the scripts to cache content, parse it and generate LaTeX templates.
* My plan is to work on a separate git repo for development of these scripts. And yet another for the generated content (most likely data caches and TeX documents)
* We should focus on generating a raw document first, that includes absolutely everything generated by the Font Bakery project. Only afterwards we'll worry about manually improving that content.
* It will be important to be able to re-run these tools/scripts to gather incremental content and update the book as the project development evolves.
* Even though the initial effort should focus on the existing checks, we may later also have a book appendix for the "New Check Proposals" that have not been implemented yet.
