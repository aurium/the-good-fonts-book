#!/usr/bin/env python3

import json
import requests
import re


def commentToMD(data):
    return (f"\n"
            f"\n"
            f"{data['body'].strip()} \n"
            f"By @{data['user']['login']}")

def issue_dabate_to_md(checkid, issue_url):
    issue_path = re.search('/([^/]+/[^/]+/issues/[0-9]+)[#?]?.*$', issue_url)[1]

    resIssue = requests.get(f'https://api.github.com/repos/{issue_path}')
    if resIssue.status_code >= 300:
        raise Exception(f'Fail to load issue {issue_path}.'
                        f' (Status {resIssue.status_code})')

    resComments = requests.get(f'https://api.github.com/repos/{issue_path}/comments')
    if resComments.status_code >= 300:
        raise Exception(f'Fail to load comments for {issue_path}.'
                        f' (Status {resComments.status_code})')

    issue = resIssue.json()
    comments = ''.join(map(commentToMD, resComments.json()))
    return (f"## {checkid}\n\n"
            f"[{issue['title'].strip()}]({issue_url})\n\n"
            f"{issue['body'].strip()}\n\n"
            f"By @{issue['user']['login']}\n\n"
            f"\n\n"
            f"{comments}"
            f"\n\n")

ISSUES = [("com.google.fonts/check/metadata/unknown_designer", 800),
          ("com.google.fonts/check/varfont_has_instances",     2127),
          ("com.google.fonts/check/description/valid_html",    2664)]

issue_md_documents = ""
for checkid, issue in ISSUES:
    md = issue_dabate_to_md(checkid, f'https://github.com/googlefonts/fontbakery/issues/{issue}')
    filename = f"issue_{issue}.md"
    open(filename, "w").write(md)
    issue_md_documents += "\\markdownInput{" + filename + "}\n"

tex_document = \
"""
\\documentclass{article}
\\usepackage[smartEllipses]{markdown}

\\begin{document}
""" + issue_md_documents + \
"""
\\end{document}
"""

open("book.tex", "w").write(tex_document)

